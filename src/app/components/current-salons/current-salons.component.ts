import { Component, OnInit } from '@angular/core';
// tslint:disable-next-line:import-blacklist
import * as Rx from 'rx-dom';
// tslint:disable-next-line:import-blacklist
import {Observable} from 'rxjs';

@Component({
  selector: 'app-current-salons',
  templateUrl: './current-salons.component.html',
  styleUrls: ['./current-salons.component.css']
})
export class CurrentSalonsComponent implements OnInit {
  salons: any;
  constructor() { }

  getSalons = () => {
    const api = Rx.DOM.ajax({
      url: 'https://api.myjson.com/bins/ldb02',
      responseType: 'json'
    });
    return api.subscribe(data => {
      this.salons = data.response;
    });
  }
  ngOnInit() {
    this.getSalons();
  }

}
