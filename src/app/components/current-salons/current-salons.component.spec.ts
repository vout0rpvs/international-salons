import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentSalonsComponent } from './current-salons.component';

describe('CurrentSalonsComponent', () => {
  let component: CurrentSalonsComponent;
  let fixture: ComponentFixture<CurrentSalonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentSalonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentSalonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
