import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.css']
})
export class PersonalInfoComponent implements OnInit {

  user = {
    name: 'Aleksey',
    lastname: 'Anisimov',
    fiap_psa: 'Lorem',
    club: 'PSA China'
  };
  delivery = {
    country: 'Ukraine',
    city: 'Kiev',
    address: 'Kravchuka Street, 32/58',
    zip: 435862
  };

  constructor() { }

  ngOnInit() {
  }

}
