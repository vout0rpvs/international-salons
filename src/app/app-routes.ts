import { Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ProfileComponent } from './components/profile/profile.component';
import { PersonalInfoComponent } from './components/personal-info/personal-info.component';
import { CurrentSalonsComponent } from './components/current-salons/current-salons.component';
import { PastSalonsComponent } from './components/past-salons/past-salons.component';
import { MyImagesComponent } from './components/my-images/my-images.component';


export const appRoutes: Routes = [
    {
        path: '',
        component: LoginComponent,
    },
    {
        path: 'signup',
        component: RegisterComponent
    },
    {
        path: 'profile',
        component: ProfileComponent,
        children: [
            {
                path: 'info',
                component: PersonalInfoComponent
            },
            {
                path: 'current-salons',
                component: CurrentSalonsComponent
            },
            {
                path: 'past-salons',
                component: PastSalonsComponent
            },
            {
                path: 'my-images',
                component: MyImagesComponent
            },
        ]
    }
];
