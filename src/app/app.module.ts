import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { LoginComponent } from './components/login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { appRoutes } from './app-routes';
import { FooterComponent } from './components/footer/footer.component';
import { RegisterComponent } from './components/register/register.component';
import { ProfileComponent } from './components/profile/profile.component';
import { PersonalInfoComponent } from './components/personal-info/personal-info.component';
import { CurrentSalonsComponent } from './components/current-salons/current-salons.component';
import { PastSalonsComponent } from './components/past-salons/past-salons.component';
import { MyImagesComponent } from './components/my-images/my-images.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    FooterComponent,
    RegisterComponent,
    ProfileComponent,
    PersonalInfoComponent,
    CurrentSalonsComponent,
    PastSalonsComponent,
    MyImagesComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
